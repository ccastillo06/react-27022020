import React from 'react';
import { Link } from 'react-router-dom';

function NotFound() {
  return (
    <div>
      <h1>Route not found</h1>
      <Link to="/">Go back home</Link>
    </div>
  );
}

export default NotFound;
