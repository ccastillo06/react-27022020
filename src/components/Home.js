import React from 'react';
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserNinja } from '@fortawesome/free-solid-svg-icons';

function Home() {
  return (
    <div>
      <h1>Welcome to Upgrade Auth!</h1>
      <Link to="/signin">
        Before continuing, register with us!{' '}
        <FontAwesomeIcon icon={faUserNinja} />
      </Link>
    </div>
  );
}

export default Home;
