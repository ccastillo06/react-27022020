import React from 'react';
import { useCookies } from 'react-cookie';

function Protected() {
  const [, , removeCookie] = useCookies(['token']);

  function handleLogout() {
    removeCookie('token');
  }

  return (
    <div>
      <h2>This is a protected route!</h2>
      <button
        style={{ display: 'block', margin: '1rem auto' }}
        type="button"
        onClick={handleLogout}
      >
        Logout
      </button>
    </div>
  );
}

export default Protected;
